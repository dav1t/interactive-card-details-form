const cardholderInput = document.getElementById("cardholder");
const cardnumberInput = document.getElementById("cardnumber");
const monthInput = document.getElementById("month");
const yearInput = document.getElementById("year");
const cvcInput = document.getElementById("cvc");

function addValidation(element) {
  element.addEventListener("focusout", () => {
    const value = element.value;
    if (value.length === 0) {
      element.parentElement.classList.add("error");
      element.classList.add("error-outline");
      return;
    }
  });

  element.addEventListener("input", () => {
    const value = element.value;
    if (value.length === 0) {
      element.parentElement.classList.add("error");
      element.classList.add("error-outline");
      return;
    }

    if (element.parentElement.classList.contains("error")) {
      element.parentElement.classList.remove("error");
      element.classList.remove("error-outline");
    }
  });
}

addValidation(cardholderInput);
addValidation(cardnumberInput);
addValidation(monthInput);
addValidation(yearInput);
addValidation(cvcInput);

document.getElementById("confirmBtn").addEventListener("click", submit);

function submit() {
  document.querySelector(".card__form").classList.toggle("d-none");
  document.querySelector(".complete__block").classList.toggle("d-none");
}
